# python -m pytest test.py
# adb shell getevent -l
# adb shell screencap -p /sdcard/screen.png
# adb pull /sdcard/screen.png
# os.system("start window_dump.xml")
import os, time

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1



# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_1():
	time.sleep(3)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml Side_Bar_dump.xml')
	f = open('Side_Bar_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	#os.system("start window_dump.xml")
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('其他') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1
	

# 2. [Screenshot] Side Bar Text
def test_2():
	os.system("adb shell screencap -p /sdcard/Side_Bar_Text.png && adb pull /sdcard/Side_Bar_Text.png .")
	pass

# 5. [Context] Categories page
def test_5():
	os.system('adb shell input tap 265 560')
	time.sleep(3)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml Categories_page_dump.xml')
	f = open('Categories_page_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	#os.system("start Categories_page_dump.xml")
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
	
	
# 6. [Screenshot] Categories page
def test_6():
	os.system("adb shell screencap -p /sdcard/Categories_page.png && adb pull /sdcard/Categories_page.png .")
	

# 3. [Context] Categories
def test_3():
	os.system('adb shell input tap 120 1720')
	time.sleep(3)
	os.system('adb shell input swipe 300 700 300 200')
	time.sleep(3)
	os.system('adb shell input tap 1003 1027')
	time.sleep(3)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml Categories_dump.xml')
	f = open('Categories_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	#os.system("start Categories_dump.xml")
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
	

# 4. [Screenshot] Categories
def test_4():
	os.system("adb shell screencap -p /sdcard/Categories.png && adb pull /sdcard/Categories.png .")
	pass
	
	
# 7. [Behavior] Search item “switch”
def test_7():
	os.system('adb shell input tap 600 135')
	time.sleep(3)
	os.system('adb shell input text "switch"')
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(5)
	os.system('adb shell input tap 500 500')
	time.sleep(5)
	os.system('adb shell input tap 90 1685')
	time.sleep(5)
	pass
	
# 8. [Behavior] Follow an item and it should be add to the list
def test_8():
	os.system('adb shell input tap 87 132')
	time.sleep(3)
	os.system('adb shell input tap 87 132')
	time.sleep(3)
	os.system('adb shell input tap 87 132')
	time.sleep(3)
	os.system('adb shell input tap 330 850')
	time.sleep(3)
	pass
	
# 9. [Behavior] Navigate tto the detail of item
def test_9():
	os.system('adb shell input tap 322 810')
	time.sleep(3)
	os.system('adb shell input swipe 300 1100 300 200')
	time.sleep(2)
	os.system('adb shell input tap 521 126')
	time.sleep(3)
	pass
	
# 10. [Screenshot] Disconnetion Screen
def test_10():
	os.system('adb shell svc data disable')
	time.sleep(1)
	os.system('adb shell svc wifi disable')
	time.sleep(1)
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./screenshot/disconnect.png')
	pass
	